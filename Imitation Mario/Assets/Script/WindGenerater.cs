﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindGenerater : MonoBehaviour
{
    public GameObject WindPrefab;
    float delta = 0;
    public float span = 2.0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;
        Debug.Log(delta);
        if (delta > span)//もし、deltaがspanより大きい場合、（この時、span秒に一回)
        {
            delta = 0;//0に戻す
            GameObject go = Instantiate(WindPrefab);//矢が作られる。
            int px = Random.Range(5, 7);//ランダムに数値内の位置にランダムに出てくるこの時、-6～7-1までの間に矢が降ってくる。
            float ppx = Random.Range(-3.4f, 4.0f);
            go.transform.position = new Vector3(px, ppx, 0);//変数のgoの初期位置をYに７の位置で入れる
        }
    }
}
