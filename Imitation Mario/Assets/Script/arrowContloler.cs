﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowContloler : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.05f, 0f, 0);//矢が落ちてくる処理

        if (transform.position.x < -8.0f)//もし、矢が地面より下に到達したら
        {
            Destroy(gameObject);//自身を破壊する

        }
        Vector2 p1 = transform.position; //矢の中心の座標

        Vector2 p2 = player.transform.position; //プレイヤーの中心座標

        Vector2 dir = p1 - p2;//ベクトルの計算


        float d = dir.magnitude;//変数ｄにVector2で求めた中心座標を引いたベクトル計算で求めたものを入れる。

        float r1 = 0.5f;//矢の当たり判定の大きさ

        float r2 = 0.8f;//プレイヤーの当たり判定の大きさ

        if (d < r1 + r2)

        {
            GameObject GameDirector = GameObject.Find("GameDirector");//GameDirectorというオブジェクトを探す
            GameDirector.GetComponent<GameDirector>().DamageHp();//GetComponentを使い、GameDirectorの中のDamageHpを呼び出している.
            Destroy(gameObject);//自身を破壊する

        }

    }
}
