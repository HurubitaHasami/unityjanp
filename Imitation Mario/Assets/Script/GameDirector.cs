﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;//UIを使うための処理
using UnityEngine.SceneManagement;
public class GameDirector : MonoBehaviour
{
    GameObject hp_bar;
    GameObject Score;
    GameObject Wind;
    GameObject Ui;
    GameObject Level;
    int A = 0;//初期は0点にしておく
    int B = 1;
    int C = 0;

    // Start is called before the first frame update
    void Start()
    {
        hp_bar = GameObject.Find("hp_bar");//hpGaugeというGameObjectを探す

        Score = GameObject.Find("Score");

       Wind = GameObject.Find("Wind");

        Ui= GameObject.Find("Ui");

        Level= GameObject.Find("Level");

    }

    // Update is called once per frame
    void Update()
    {
        Score.GetComponent<Text>().text = "Score" +A+ "点";//常にスコアA点というものを表示しておく

        Level.GetComponent<Text>().text = "Lv."+B;//常にスコアA点というものを表示しておく
    }

    public void Score2()
    {
        A += 85;//Aという変数に90ずつ足していく
        C += 100;
        Score.GetComponent<Text>().text = "Score" +A+ "点";//足した後の点数を表示する

        if (C > 300)
        {
            LevelUP();
            C = 0;
        }
    }

    public void LevelUP()
    {
        B += 1;
        Level.GetComponent<Text>().text = "Lv." + B;//足した後の点数を表示する
    }
    public void DamageHp()//publicをつけると外部にも公開可能　つまり、この場合はほかのスクリプトでも編集可能になる状態に
    {
        hp_bar.GetComponent<Image>().fillAmount -= 0.1f;//hpバーが減っていく。
        if(hp_bar.GetComponent<Image>().fillAmount == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }

    public void RifleshHp()
    {
        hp_bar.GetComponent<Image>().fillAmount += 0.1f;
    }


}
